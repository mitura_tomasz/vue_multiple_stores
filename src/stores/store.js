import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


const moduleA = {
  state: {
    message: 'Hello from moduleA',
    count: 0
  },
  mutations: {

  }, 
  actions: {

  },
  getters: {
    message(state) {
      return state.message
    }
  }
}

const moduleB = {
  namespaced: true,
  state: {
    message: 'Hello from moduleB',
    count: 0
  },
  mutations: {

  }, 
  actions: {

  },
  getters: {
    message(state) {
      return state.message
    }
  }
}

const store = new Vuex.Store({
  modules: {
    a: moduleA,
    b: moduleB
  }
})

export default store